---
title: "Is het nou panne- of pannenkoek?"
date: 2019-03-23T04:28:21+06:00
image: images/blog/blog-img-1.jpg
type: "post"
---

Pannekoek als je het ons vraagt! Maar we hebben het onderzocht. Het blijkt dat tot 1995 'pannekoek' dé officiële spelling was van het woord. Daarna, vraag je je af? Tja, toen werd het 'pannenkoek'. Maar vrees niet: [Onze taal geeft aan dat 'pannekoek' ook gebruikt mag worden](https://onzetaal.nl/taaladvies/pannekoek-pannenkoek), en het NRC kopte met ["Pannekoek mag weer, pannenkoek ook"](https://www.nrc.nl/nieuws/2006/08/15/pannekoek-mag-weer-pannenkoek-ook-11176805-a1305975).

\
Onze taal schrijft het volgende:

> In de praktijk komt pannekoek nog steeds geregeld voor. Veel mensen beschouwen het als een vaste benaming voor deze lekkernij. Het is een min of meer ‘versteende’ vorm. Pannenkoek is voor sommigen te letterlijk. Voor anderen lijkt het alsof bedoeld is dat de koek ook in meer dan één pan gebakken zou kunnen worden.

Er is maar één uitzondering: mensen die werken bij de overheid of in het onderwijs. Voor deze mensen is de officiele spelling namelijk verplicht.

\
Bij de _ikwilopmijnpannekoek.nl_ zijn we echter de kwaatste niet, en zullen wij op beide verzoeken reageren. Vraag dus gerust om een 'pannekoek' of een 'pannenkoek'!