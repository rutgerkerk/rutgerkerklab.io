---
title: "Ode aan de pannekoekenplant"
date: 2019-05-23T12:28:47+06:00
image: images/blog/blog-img-2.jpg
type: "post"
---

De pannekoekenplant! Een leuk groen plantje wat makkelijk te stekken is. Tegenwoordig heeft bijna iedereen er één in huis. En het is ook een mooie plant! En niet in de laatste plaats omdat de bladeren op pannekoeken lijken! Oké, de plant heet niet echt zo, de correcte naam is 'Pilea peperomioides'.

\
[Wikpedia schrijft het volgende:](https://nl.wikipedia.org/wiki/Pilea_peperomioides) 

> Dit geslacht behoort tot de brandnetelfamilie (Urticaceae). Pilea peperomioides komt van nature alleen maar in China voor, in het bergachtige gebied van de provincie Yunnan. Van hier bracht in het begin van de 20e eeuw de Schotse botanicus George Forrest (1873-1932) de plant mee naar Europa. De Duitse botanicus Friedrich Ludwig Emil Diels (1874-1945) gaf haar in 1912 de naam peperomioides (op Peperomia gelijkend).

Nog een leuk feitje: In het engels wordt de plant ook wel 'Chinese money plant' genoemd. Ik houd het echter bij 'pannekoekenplant'. Wil je ook zo een plantje? Dan heb je geluk! Elke deelnemer van de pannekoekenfeesten krijgt een stekje mee naar huis. Leuk!