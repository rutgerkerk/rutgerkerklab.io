---
title: "Pannekoeken over de hele wereld"
date: 2019-04-23T12:23:10+06:00
image: images/blog/blog-img-3.jpg
type: "post"
---

Wie is eigenlijk de uitvinden van de pannekoek? De schrijver van dit blog heeft geen idee. [Wikipedia schrijft over de oude grieken](https://en.wikipedia.org/wiki/Pancake#History). Niet alleen bestaan pannekoeken al heen lang, ook zijn er vele variaties! Iedereen is wel bekend met de Franse crêpe, en ook de American pancake komt in menig Amerikaanse film voorbij. Maar ook de Nederlandse poffertjes vallen hieronder. Grappig genoeg is de [Dutch baby pancake](https://en.wikipedia.org/wiki/Dutch_baby_pancake) dan weer een grote poffer.

\
Ben je benieuwd naar nog meer variaties? Bezoek dan de [lijst van pannekoeken](https://en.wikipedia.org/wiki/List_of_pancakes) op wikpedia.