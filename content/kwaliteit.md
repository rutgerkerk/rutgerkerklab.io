---
title: "Wat maakt een goede pannekoek?"
date: 2019-06-23T12:30:01+06:00
image: images/blog/blog-img-4.jpg
type: "post"
---

Wij hebben het antwoord! Het is namelijk niet zo simpel als je denkt...

\
Het begint allemaal met de basisingredïenten, bloem, melk en eieren. Beetje zout er bij, boter om in te bakken. Maar dan? Het geheim van een goede pannekoek zit hem in de persoonlijk aanpak! Luister naar de eter: heeft iemand enorm zin in appel? Dan doen we dat er op! Liever kaas? Gebruik dat! Maar het stopt niet bij de ingredienten, ook de gezelligheid speelt mee. Maak een praatje, zet een muziekje op. Stel (jezelf) continue de vraag: 'voor wat soort pannekoek sta je midden in de nacht op?'.

\
Werkt je pan niet mee? Zijn de verse ingredienten op? Dan nog kan je er een succes van maken! Wij hebben daarvoor twee simpele stappen:

1. Geef de eters veel bier of andere alcoholische drank, dan valt de smaak van de pannekoek minder op. 
2. Geef de eters een [pannekoekenplant mee naar huis](/plant).

Tot nu toe hebben wij met die tactiek nog geen klachten gehad ;)